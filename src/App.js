import React, { useState, useEffect } from 'react'
import './App.css'
import Form from "./components/Form"
import TodoList from "./components/TodoList"

function App() {
  const [inputText, setInputText] = useState('')
  const [todos, setTodos] = useState([])
  const [status, setStatus] = useState('all')
  const [filteredTodos, setFilteredTodos] = useState([])

  // Run once when the app starts
  useEffect(() => {
    let todosFromLocalStorage = localStorage.getItem('todos')

    localStorage.getItem('todos') === null ?
        localStorage.setItem('todos', JSON.stringify([])) :
        setTodos(JSON.parse(todosFromLocalStorage))
  }, [])

  useEffect(() => {
    const filterHandler = () => {
      switch (status) {
        case 'completed':
          setFilteredTodos(todos.filter(todo => todo.completed))
          break
        case 'uncompleted':
          setFilteredTodos(todos.filter(todo => !todo.completed))
          break
        default:
          setFilteredTodos(todos)
      }
    }

    const saveTodosOnLocalStorage = () => {
      localStorage.getItem('todos') === null ?
        localStorage.setItem('todos', JSON.stringify([])) :
        localStorage.setItem('todos', JSON.stringify(todos))
    }

    filterHandler()
    saveTodosOnLocalStorage()
  }, [
    todos, status
  ])

  return (
    <div className="App">
      <header>
        <h1>Jay's Todo List</h1>
      </header>
      <Form
        setInputText={setInputText}
        todos={todos}
        setTodos={setTodos}
        inputText={inputText}
        status={status}
        setStatus={setStatus}
      />
      <TodoList todos={todos} setTodos={setTodos} filteredTodos={filteredTodos} />
    </div>
  )
}

export default App
